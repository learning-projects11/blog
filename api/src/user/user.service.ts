import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    try {
      return await this.userRepository.save(createUserDto);
    } catch (err) {
      throw new NotFoundException(err);
    }
  }

  async findAll() {
    try {
      return await this.userRepository.find();
    } catch (err) {
      throw new NotFoundException(err);
    }
  }

  async findOne(id: number) {
    try {
      return await this.userRepository.findOneBy({ id: id });
    } catch (err) {
      throw new NotFoundException(err);
    }
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    try {
      return await this.userRepository.update(id, updateUserDto);
    } catch (err) {
      throw new NotFoundException(err);
    }
  }

  async remove(id: number) {
    try {
      return await this.userRepository.softDelete(id);
    } catch (err) {
      throw new NotFoundException(err);
    }
  }
}
